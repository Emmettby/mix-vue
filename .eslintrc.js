module.exports = {
    env: {
        browser: true,
        es6: true,
    },
    extends: [
        'airbnb-base',
        'plugin:vue/recommended',
    ],
    globals: {
        Atomics: 'readonly',
        SharedArrayBuffer: 'readonly',
    },
    // parserOptions: {
    //     ecmaVersion: 2018,
    //     sourceType: 'module',
    // },
    'parserOptions': {
        parser: 'babel-eslint',
        ecmaVersion: 2018,
        sourceType: 'module',
    },
    plugins: [
        'vue',
    ],
    rules: {
        'arrow-parens': ['error', 'always'],
        'array-bracket-spacing': ['error', 'never'],
        'comma-dangle': ["error", "always-multiline"],
        'indent': ['error', 4],
        'linebreak-style': ['error', 'unix'],
        'max-len': 'off',
        'no-cond-assign': ['error', 'always'],
        'no-empty': ['error', { 'allowEmptyCatch': true }],
        'no-param-reassign': ['error', { 'props': false }],
        'no-plusplus': 'off',
        'prefer-destructuring': 'off',
        'radix': ['error', 'as-needed'],
        'space-in-parens': ['error', 'never'],
        'vue/html-indent': ['error', 4],
        'vue/no-v-html': 'off',
    },
};